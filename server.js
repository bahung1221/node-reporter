const createError = require('http-errors')
const express = require('express')
const logger = require('morgan')
const CronJob = require('cron').CronJob
const cronJob = require('./utils/cronJob')
const axios = require('axios')
const app = express()

app.use(logger('dev'))

// Cron jobs
new CronJob('00 00 22 * * *', async function() {
  console.log('Run mailer')
  let logs = await cronJob.readLogsDir(),
    time = (new Date()).toLocaleDateString('de-DE', {
      year: 'numeric', month: 'numeric', day: 'numeric',
      hour: 'numeric', minute: 'numeric', second: 'numeric',
      timeZone: 'Asia/Bangkok'
    }),
    mailOptions = {
      auth: {
        MAIL_USERNAME: process.env.MAIL_USERNAME,
        MAIL_PASSWORD: process.env.MAIL_PASSWORD
      },
      info: {
        MAIL_FROM: process.env.MAIL_FROM,
        MAIL_TO: process.env.MAIL_TO
      },
      subject: `Daily report - ofile.io - ${time}`,
      text: logs
    }

  let mailApi = axios.create({
    baseURL: process.env.MAIL_SERVICE_URL,
    timeout: 10000,
    withCredentials: true,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  })

  mailApi.post('/mail', mailOptions)
    .then(info => {
      console.log(info)
    })
    .catch(e => {
      console.log(e)
    })

}, null, true, 'Asia/Bangkok')

// new CronJob('* * * * *', async function() {
//   console.log('Run system checker')
//   let systemLogs = await cronJob.checkDiskSpace()
//   console.log(systemLogs)
// }, null, true, 'Asia/Bangkok')

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.sendStatus(err.status || 500)
})

module.exports = app
