const fs = require('fs')
const LineByLineReader = require('./lineByLine')
const path = require('path')
const mkdirp = require('mkdirp')

function processFileLineByLine(filePath) {
  // If file not exists, create new file
  if (!fs.existsSync(filePath)) {
    return null
  }

  let rl = new LineByLineReader(filePath)

  return rl
}

module.exports = {
  processFileLineByLine: processFileLineByLine
}
