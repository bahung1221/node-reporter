// TODO: Refactor all, too many cheat

const path = require('path')
const fs = require('fs')
const helper = require('./helper')
const moment = require('moment')
const microstats = require('microstats')
const os = require('os')

function checkDiskSpace() {
  let memory = 'MEMORY: \n',
    cpu = 'CPU: \n',
    disk = 'DISK: \n',
    count = 0,
    options = {}

    // Check os type
  if (os.type() === 'Linux') {
    options = {
      frequency: 'once',
      diskalert: {
        used: '>70%',
        mount: '/dev/sda'
      },
      memoryalert: {
        used: '>70%'
      },
      cpualert: {
        load: '>70%'
      }
    }
  } else if (os.type() === 'Windows_NT') {
    options = {
      frequency: 'once',
      diskalert: {
        used: '>70%',
        filesystem: 'C:'
      },
      memoryalert: {
        used: '>70%'
      },
      cpualert: {
        load: '>70%'
      }
    }
  }

  return new Promise(function (resolve, reject) {
    // Event emits
    microstats.on('memory', function(value) {
      memory += `\tUsed: ${value.usedpct}%\n`
      memory += `\tTotal: ${value.total} bytes\n`
      memory += `\tFree: ${value.free} bytes\n`
      count++
      if (count === 4) {
        resolve(memory + cpu + disk)
      }
    })
    microstats.on('cpu', function(value) {
      cpu += `\tUsed: ${value.loadpct}\n`
      count++
      if (count === 4) {
        resolve(memory + cpu + disk)
      }
    })
    microstats.on('disk', function(value) {
      disk += `\tFile system: ${value.filesystem}\n`
      disk += `\tUsed: ${value.usedpct}%\n`
      disk += `\tTotal: ${value.total} bytes\n`
      disk += `\tFree: ${value.free} bytes\n`
      count++
      if (count === 4) {
        resolve(memory + cpu + disk)
      }
    })

    microstats.start(options, function(err) {
      if(err) {
        console.log(err)
        reject(err)
      }
    })
  })
}

/**
 * Clean tmp dir, check life time of file then delete it if expired
 */
function readLogsDir() {
  let downloadKeycode = 'Downloaded by key code: \n',
    downloadURL = 'Downloaded by URL: \n',
    deleted = 'Deleted: \n',
    uploaded = 'Uploaded: \n',
    lang = 'Language selected: \n',
    uploadTotal = 0,
    downloadUrlTotal = 0,
    downloadKeyCodeTotal = 0,
    deleteTotal = 0,
    langTotal = 0,
    time = moment.tz('Asia/Bangkok'),
    count = 0,
    logsFileCount = 0

  // Declare root path of logs folder
  // let pathLogsFolder = '/var/www/vhosts/tomato-transfer/logs'
  let pathLogsFolder = path.join(__dirname, '../../ofile.io/logs')

  return new Promise(function (resolve, reject) {
    // Read tmp folder
    fs.readdir(pathLogsFolder, (err, files) => {
      if (err) {
        return reject(err)
      }
      logsFileCount = files.length - 2
      console.log(logsFileCount)
      // Loop over file list
      files.forEach(file => {
        if (!file.includes('out') && !file.includes('err')) {
          //Get path of child file/directory
          let filePath = path.join(pathLogsFolder, file)
          fs.stat(filePath, (err, stat) => {
            if (err) {
              console.error(err)
              return
            }

            let rl = helper.processFileLineByLine(filePath)

            rl.on('line', function(line) {
              if (checkTime(line, time)) {
                if (file.includes('downloadKeycode')) {
                  downloadKeycode += `\t${line}\n`
                  downloadKeyCodeTotal++
                } else if (file.includes('downloadURL')) {
                  downloadURL += `\t${line}\n`
                  downloadUrlTotal++
                } else if (file.includes('deleted')) {
                  deleted += `\t${line}\n`
                  deleteTotal++
                } else if (file.includes('uploaded')) {
                  uploaded += `\t${line}\n`
                  uploadTotal++
                } else if (file.includes('lang')) {
                  lang += `\t${line}\n`
                  langTotal++
                }
              }
            })

            rl.on('error', function(e) {
              console.error(e)
            })

            rl.on('end', function() {
              count++
              if (count === logsFileCount || count === 5) {
                resolve([
                  downloadKeycode + downloadURL + uploaded + deleted + lang,
                  `Download total: ${downloadUrlTotal + downloadKeyCodeTotal}\n`
                  + `Upload total: ${uploadTotal}\n`
                  + `Delete total: ${deleteTotal}\n`
                ])
              }
            })
          })
        }
      })
    })
  })
}

/**
 * Validate time of line
 * @param line
 * @param time
 * @returns {boolean}
 */
function checkTime(line, time) {
  let year = time.year(),
    month = time.month() + 1,
    date = time.date(),
    splitedLine = line.split(':')

  splitedLine.pop()

  if (splitedLine[splitedLine.length - 1] == ' D') {
    // TODO: remove this cheat for windows
    splitedLine.pop()
  }
  let lineTime = moment(splitedLine.join(':')).tz('Asia/Bangkok'),
    lineYear = lineTime.year(),
    lineMonth = lineTime.month() + 1,
    lineDate = lineTime.date()

  if (lineDate === date && lineMonth === month && lineYear === year) {
    return true
  }

  return false
}

module.exports = {
  readLogsDir: readLogsDir,
  checkDiskSpace: checkDiskSpace
}
